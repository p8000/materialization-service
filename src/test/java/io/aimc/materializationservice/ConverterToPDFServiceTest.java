package io.aimc.materializationservice;

import io.aimc.materializationservice.model.Letter;
import io.aimc.materializationservice.service.ConverterToPDFService;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ActiveProfiles("test")
@SpringBootTest(classes = ConverterToPDFService.class)
class ConverterToPDFServiceTest {
    @Autowired
    private ConverterToPDFService converter;
    @Value("${files.directory.pdf}")
    private String directory;

    @Test
    public void createPdfFileTest() throws IOException {
        Letter letter = new Letter();
        UUID shipmentSourceId = UUID.randomUUID();
        letter.setId(shipmentSourceId);
        letter.setReceiver("test");
        letter.setSender("test");
        letter.setContent("test");
        letter.setPostOfficeName("test");
        letter.setPostOfficeAddress("test");
        List<Letter> letters = List.of(letter);

        converter.createPdfFiles(letters);

        PDDocument actualDocument = PDDocument.load(new File(directory + shipmentSourceId + ".pdf"));
        String actualText = new PDFTextStripper().getText(actualDocument);

        assertTrue(actualText.contains("Sender: test"));
        assertTrue(actualText.contains("Receiver: test"));
        assertTrue(actualText.contains("Content:"));
        assertTrue(actualText.contains("  test"));
        assertTrue(actualText.contains("Post office: test"));
        assertTrue(actualText.contains("Address: test"));
    }


}
