package io.aimc.materializationservice.model;

import lombok.Data;

import java.util.UUID;

@Data
public class Letter {
    private UUID id;
    private String sender;
    private String content;
    private String receiver;
    private String postOfficeName;
    private String postOfficeAddress;
}
