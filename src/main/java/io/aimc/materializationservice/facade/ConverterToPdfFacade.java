package io.aimc.materializationservice.facade;

import io.aimc.materializationservice.client.ShipmentClient;
import io.aimc.materializationservice.dto.PortionDto;
import io.aimc.materializationservice.model.Letter;
import io.aimc.materializationservice.service.ConverterToPDFService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ConverterToPdfFacade {
    private final ShipmentClient shipmentClient;
    private final ConverterToPDFService converterService;

    public void createPdfFile(PortionDto portionDto) {
        List<Letter> letters = shipmentClient.getAllByIds(portionDto.getShipmentIds());
        converterService.createPdfFiles(letters);
    }
}
