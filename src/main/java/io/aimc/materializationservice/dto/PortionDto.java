package io.aimc.materializationservice.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Data
public class PortionDto {
    private UUID id;
    private List<UUID> shipmentIds;
}
