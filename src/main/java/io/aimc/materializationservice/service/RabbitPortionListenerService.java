package io.aimc.materializationservice.service;

import io.aimc.materializationservice.dto.PortionDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class RabbitPortionListenerService {
    private final PdfService pdfService;

    @RabbitListener(queues = "${spring.rabbitmq.portion-queue}")
    public void getPortion(PortionDto portionDto) {
        log.info("receive from rabbit: {}", portionDto.toString());
        pdfService.createPdfFiles(portionDto);
    }

}
