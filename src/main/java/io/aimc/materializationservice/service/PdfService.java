package io.aimc.materializationservice.service;

import io.aimc.materializationservice.dto.PortionDto;

public interface PdfService {
    void createPdfFiles(PortionDto portionDto);
}
