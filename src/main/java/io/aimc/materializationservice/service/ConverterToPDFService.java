package io.aimc.materializationservice.service;

import io.aimc.materializationservice.exception.PdfGenerationException;
import io.aimc.materializationservice.model.Letter;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Service
@Slf4j
public class ConverterToPDFService {
    @Value("${files.directory.pdf}")
    private String directory;

    public void createPdfFiles(List<Letter> letters) {
        new File(directory).mkdir();
        letters.forEach(shipment -> {
            try {
                PDDocument document = formPdfFile(shipment);
                document.save(directory + shipment.getId() + ".pdf");
                document.close();
            } catch (IOException e) {
                log.error("file creation error", e);
                throw new PdfGenerationException("generate pdf failed", e);
            }
        });
    }

    private PDDocument formPdfFile(Letter letter) throws IOException {
        PDDocument doc = new PDDocument();
        PDPage page = new PDPage();
        doc.addPage(page);
        try (PDPageContentStream content = new PDPageContentStream(doc, page)) {
            content.beginText();
            content.setFont(PDType1Font.TIMES_ROMAN, 14);
            content.setLeading(14.5f);
            content.newLineAtOffset(25, 700);
            content.showText("Sender: " + letter.getSender());
            content.showText("  Receiver: " + letter.getReceiver());
            content.newLine();
            content.showText("Content:");
            content.newLine();
            content.showText("  " + letter.getContent());
            content.newLine();
            content.showText("Post office: " + letter.getPostOfficeName());
            content.showText(" Address: " + letter.getPostOfficeAddress());
            content.endText();
        }
        return doc;
    }
}
