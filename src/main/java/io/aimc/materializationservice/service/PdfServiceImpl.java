package io.aimc.materializationservice.service;

import io.aimc.materializationservice.dto.PortionDto;
import io.aimc.materializationservice.facade.ConverterToPdfFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PdfServiceImpl implements PdfService {
    private final ConverterToPdfFacade converter;

    @Override
    public void createPdfFiles(PortionDto portionDto) {
        converter.createPdfFile(portionDto);
    }
}
