package io.aimc.materializationservice.service;

import io.aimc.materializationservice.dto.PortionDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class KafkaPortionListenerService {
    private final PdfService pdfService;

    @KafkaListener(topics = "${spring.kafka.portion-topic}")
    public void getPortion(PortionDto portionDto) {
        log.info("receive from kafka: {}", portionDto.toString());
        pdfService.createPdfFiles(portionDto);
    }
}

