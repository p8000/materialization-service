package io.aimc.materializationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaterializationServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(MaterializationServiceApplication.class, args);
    }

}
