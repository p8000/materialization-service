package io.aimc.materializationservice.client;

import io.aimc.materializationservice.model.Letter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.UUID;

import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

@Service
@RequiredArgsConstructor
public class ShipmentClientImpl implements ShipmentClient {
    private final RestTemplate restTemplate;

    @Value("${rest.template.service.shipmentreceiver}")
    private String shipmentReceiverUrl;


    @Override
    public List<Letter> getAllByIds(List<UUID> ids) {
        UriComponentsBuilder url = fromHttpUrl(shipmentReceiverUrl).queryParam("ids", ids);
        URI uri = url.build().encode().toUri();
        ResponseEntity<List<Letter>> response = restTemplate.exchange(uri,
                HttpMethod.GET, null, new ParameterizedTypeReference<>() {
                });
        return response.getBody();
    }
}
