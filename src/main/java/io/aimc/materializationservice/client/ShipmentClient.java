package io.aimc.materializationservice.client;

import io.aimc.materializationservice.model.Letter;

import java.util.List;
import java.util.UUID;

public interface ShipmentClient {
    List<Letter> getAllByIds(List<UUID> ids);
}
